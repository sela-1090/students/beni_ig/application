from flask import Flask, request, render_template
import psycopg2
import yaml
import os

app = Flask(__name__)

def read_db_config_from_env():
    db_config = {
        'host': os.environ.get('DB_HOST', 'localhost'),
        'port': os.environ.get('DB_PORT', '5432'),
        'name': os.environ.get('DB_NAME', 'postgres'),
        'user': os.environ.get('DB_USER', 'admindb'),
        'password': os.environ.get('DB_PASSWORD', 'admin123')
    }
    return db_config

def connect_db():
    db_config = read_db_config_from_env()
    try:
        connection = psycopg2.connect(
            host=db_config['host'],
            port=db_config['port'],
            database=db_config['name'],
            user=db_config['user'],
            password=db_config['password']
        )
        return connection
    except psycopg2.Error as e:
        print("Error connecting to the database:", e)
        return None

@app.route('/', methods=['GET','POST'])
def index():
    return render_template('index.html',)

@app.route('/search', methods=['POST'])
def search_movies():
    movie_name = request.form['movie_name']
    conn = connect_db()
    if conn:
        cursor = conn.cursor()
        cursor.execute('SELECT name, description, actors FROM movies WHERE name LIKE %s', ('%' + movie_name + '%',))
        results = cursor.fetchall()
        conn.close()
    else:
        return("error connecting to db")
    return render_template('search_results.html', movies=results)
if __name__ == '__main__':
    app.run(debug=True)
